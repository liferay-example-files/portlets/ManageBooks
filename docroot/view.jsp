<%@include file="/init.jsp" %>


<b>Library Manager</b>


<liferay-ui:header title="view-books" backURL="<%= redirect %>" />

<liferay-portlet:renderURL var="editBookURL">
		<liferay-portlet:param name="mvcPath" value="/edit_book.jsp" />
		<liferay-portlet:param name="redirect" value="<%= currentURL %>" />
</liferay-portlet:renderURL>

<aui:button name="add" onClick="<%= editBookURL %>" value="add-book" />

<liferay-ui:search-container emptyResultsMessage="no-books-were-found" >

	<liferay-ui:search-container-results>

		<%
			int start = QueryUtil.ALL_POS;
			int end = QueryUtil.ALL_POS;

			if(showPagination) {
				start = searchContainer.getStart();
				end = searchContainer.getEnd();
			}

			if (permissionChecker.isCompanyAdmin()) {

 				results = BookLocalServiceUtil.getBooks(start, end);
 				total = BookLocalServiceUtil.getBooksCount();


 			}
			else {

				results = BookLocalServiceUtil.getBooks(
						themeDisplay.getCompanyId(),
						themeDisplay.getScopeGroupId(),
						start,
						end);

				total = BookLocalServiceUtil.getBooksCount(
						themeDisplay.getCompanyId(),
						themeDisplay.getScopeGroupId());

			}

			pageContext.setAttribute("results", results);
			pageContext.setAttribute("total", total);

			System.out.println("results per page = " + results.size());
			System.out.println("total results = " + total);

		%>
	</liferay-ui:search-container-results>


		<!-- modelVar : Bean da cui prendere la proprietÓ specificata in keyProperty  -->

			<liferay-ui:search-container-row
				className="it.unisef.managebooks.model.Book"
				keyProperty="bookId"
				modelVar="book"
			>

				<liferay-ui:search-container-column-text
					name="bookId"
					property="bookId"
				/>

				<liferay-ui:search-container-column-text
					name="userId"
					property="userId"
				/>

				<liferay-ui:search-container-column-text
					name="title"
					property="title"
				/>

				<liferay-ui:search-container-column-text
					name="author"
					property="author"
				/>

				<liferay-ui:search-container-column-text
					name="publisher"
					property="publisher"
				/>

				<liferay-ui:search-container-column-text
					name="pages"
					property="pages"
				/>

				<liferay-ui:search-container-column-text
					name="price"
					property="price"
				/>

				<liferay-ui:search-container-column-text
					name="description"
					value="<%= book.getDescription() %>"
				/>


				<liferay-ui:search-container-column-jsp
					align="right"
					path="/book_action.jsp"
				/>

			</liferay-ui:search-container-row>


	<liferay-ui:search-iterator paginate="<%= showPagination %>"/>

</liferay-ui:search-container>