package it.unisef.managebooks.hook.listeners;

import it.unisef.managebooks.service.BookLocalServiceUtil;

import com.liferay.portal.ModelListenerException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.model.BaseModelListener;
import com.liferay.portal.model.User;

public class UserListener extends BaseModelListener<User> {

	@Override
	public void onBeforeRemove(User model) throws ModelListenerException {

		try {
			BookLocalServiceUtil.deleteUserBooks(model.getUserId());

			_log.info("Titoli dell'utente " + model.getUserId() + " cancellati.");
		}
		catch (Exception e) {
			_log.info("Errore durante la cancellazione dei Titoli dell'utente" +
						model.getUserId() );
		}

	};


	private static Log _log = LogFactoryUtil.getLog(UserListener.class);

}

