package it.unisef.managebooks.portlet;

import it.unisef.managebooks.model.Book;
import it.unisef.managebooks.service.BookLocalServiceUtil;
import it.unisef.managebooks.util.WebKeys;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextFactory;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.util.bridges.mvc.MVCPortlet;

public class ManageBooksPortlet extends MVCPortlet {


	public void updateBook(
			ActionRequest request, ActionResponse response)
		throws Exception {

		ServiceContext serviceContext = ServiceContextFactory.getInstance(
										Book.class.getName(), request);

		long bookId = ParamUtil.getLong(request, "bookId");
		String title = ParamUtil.getString(request, "title");
		String author = ParamUtil.getString(request, "author");
		String description = ParamUtil.getString(request, "description");
		String publisher = ParamUtil.getString(request, "publisher");

		int pageNumber = ParamUtil.getInteger(request, "pages");
		double price = ParamUtil.getDouble(request, "price");

		ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(
										WebKeys.THEME_DISPLAY);

		long userId = themeDisplay.getUserId();

		if(bookId > 0 ) {
			BookLocalServiceUtil.updateBook(bookId, title, author,
					description, publisher, pageNumber, price);
		}
		else {
			BookLocalServiceUtil.addBook(
					userId, title, author, description, publisher,
					pageNumber, price, serviceContext);
		}

	}

	public void deleteBook(ActionRequest request, ActionResponse response)
			throws Exception {


		long bookId = ParamUtil.getLong(request, "bookId");

		BookLocalServiceUtil.deleteBook(bookId);

	}

	public void render(RenderRequest request, RenderResponse response)
			throws PortletException, IOException {


			try {
				Book book = null;

				long bookId = ParamUtil.getLong(request, "bookId");

				if(bookId > 0) {
					book = BookLocalServiceUtil.getBook(bookId);

				}

				request.setAttribute(WebKeys.BOOK, book);
				System.out.println("render() bookId = " + bookId);

			}
			catch(Exception e ) {
				throw new PortletException();
			}


			super.render(request, response);
	}

	public static Log _log = LogFactoryUtil.getLog(ManageBooksPortlet.class);
}
