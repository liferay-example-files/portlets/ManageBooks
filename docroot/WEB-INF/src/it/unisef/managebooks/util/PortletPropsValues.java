package it.unisef.tasks.util;

import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.util.portlet.PortletProps;

public class PortletPropsValues {


	public static final boolean TASKS_CHECK_ENABLED = GetterUtil.getBoolean(
							PortletProps.get(PortletPropsKeys.TASKS_CHECK_ENABLED));


}
