create index IX_319AD7B on MB_Book (companyId, groupId);
create index IX_CC3FA651 on MB_Book (groupId);
create index IX_9EB2D5B3 on MB_Book (userId);
create index IX_4739BB5 on MB_Book (userId, companyId, groupId);
create index IX_568C5857 on MB_Book (userId, groupId);