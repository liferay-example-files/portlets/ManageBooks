/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package it.unisef.managebooks.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Book}.
 * </p>
 *
 * @author    Giulio Piemontese
 * @see       Book
 * @generated
 */
public class BookWrapper implements Book, ModelWrapper<Book> {
	public BookWrapper(Book book) {
		_book = book;
	}

	public Class<?> getModelClass() {
		return Book.class;
	}

	public String getModelClassName() {
		return Book.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("bookId", getBookId());
		attributes.put("companyId", getCompanyId());
		attributes.put("userId", getUserId());
		attributes.put("groupId", getGroupId());
		attributes.put("userName", getUserName());
		attributes.put("createDate", getCreateDate());
		attributes.put("modifiedDate", getModifiedDate());
		attributes.put("title", getTitle());
		attributes.put("author", getAuthor());
		attributes.put("description", getDescription());
		attributes.put("publisher", getPublisher());
		attributes.put("pages", getPages());
		attributes.put("price", getPrice());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long bookId = (Long)attributes.get("bookId");

		if (bookId != null) {
			setBookId(bookId);
		}

		Long companyId = (Long)attributes.get("companyId");

		if (companyId != null) {
			setCompanyId(companyId);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}

		String userName = (String)attributes.get("userName");

		if (userName != null) {
			setUserName(userName);
		}

		Date createDate = (Date)attributes.get("createDate");

		if (createDate != null) {
			setCreateDate(createDate);
		}

		Date modifiedDate = (Date)attributes.get("modifiedDate");

		if (modifiedDate != null) {
			setModifiedDate(modifiedDate);
		}

		String title = (String)attributes.get("title");

		if (title != null) {
			setTitle(title);
		}

		String author = (String)attributes.get("author");

		if (author != null) {
			setAuthor(author);
		}

		String description = (String)attributes.get("description");

		if (description != null) {
			setDescription(description);
		}

		String publisher = (String)attributes.get("publisher");

		if (publisher != null) {
			setPublisher(publisher);
		}

		Integer pages = (Integer)attributes.get("pages");

		if (pages != null) {
			setPages(pages);
		}

		Double price = (Double)attributes.get("price");

		if (price != null) {
			setPrice(price);
		}
	}

	/**
	* Returns the primary key of this book.
	*
	* @return the primary key of this book
	*/
	public long getPrimaryKey() {
		return _book.getPrimaryKey();
	}

	/**
	* Sets the primary key of this book.
	*
	* @param primaryKey the primary key of this book
	*/
	public void setPrimaryKey(long primaryKey) {
		_book.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the book ID of this book.
	*
	* @return the book ID of this book
	*/
	public long getBookId() {
		return _book.getBookId();
	}

	/**
	* Sets the book ID of this book.
	*
	* @param bookId the book ID of this book
	*/
	public void setBookId(long bookId) {
		_book.setBookId(bookId);
	}

	/**
	* Returns the company ID of this book.
	*
	* @return the company ID of this book
	*/
	public long getCompanyId() {
		return _book.getCompanyId();
	}

	/**
	* Sets the company ID of this book.
	*
	* @param companyId the company ID of this book
	*/
	public void setCompanyId(long companyId) {
		_book.setCompanyId(companyId);
	}

	/**
	* Returns the user ID of this book.
	*
	* @return the user ID of this book
	*/
	public long getUserId() {
		return _book.getUserId();
	}

	/**
	* Sets the user ID of this book.
	*
	* @param userId the user ID of this book
	*/
	public void setUserId(long userId) {
		_book.setUserId(userId);
	}

	/**
	* Returns the user uuid of this book.
	*
	* @return the user uuid of this book
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _book.getUserUuid();
	}

	/**
	* Sets the user uuid of this book.
	*
	* @param userUuid the user uuid of this book
	*/
	public void setUserUuid(java.lang.String userUuid) {
		_book.setUserUuid(userUuid);
	}

	/**
	* Returns the group ID of this book.
	*
	* @return the group ID of this book
	*/
	public long getGroupId() {
		return _book.getGroupId();
	}

	/**
	* Sets the group ID of this book.
	*
	* @param groupId the group ID of this book
	*/
	public void setGroupId(long groupId) {
		_book.setGroupId(groupId);
	}

	/**
	* Returns the user name of this book.
	*
	* @return the user name of this book
	*/
	public java.lang.String getUserName() {
		return _book.getUserName();
	}

	/**
	* Sets the user name of this book.
	*
	* @param userName the user name of this book
	*/
	public void setUserName(java.lang.String userName) {
		_book.setUserName(userName);
	}

	/**
	* Returns the create date of this book.
	*
	* @return the create date of this book
	*/
	public java.util.Date getCreateDate() {
		return _book.getCreateDate();
	}

	/**
	* Sets the create date of this book.
	*
	* @param createDate the create date of this book
	*/
	public void setCreateDate(java.util.Date createDate) {
		_book.setCreateDate(createDate);
	}

	/**
	* Returns the modified date of this book.
	*
	* @return the modified date of this book
	*/
	public java.util.Date getModifiedDate() {
		return _book.getModifiedDate();
	}

	/**
	* Sets the modified date of this book.
	*
	* @param modifiedDate the modified date of this book
	*/
	public void setModifiedDate(java.util.Date modifiedDate) {
		_book.setModifiedDate(modifiedDate);
	}

	/**
	* Returns the title of this book.
	*
	* @return the title of this book
	*/
	public java.lang.String getTitle() {
		return _book.getTitle();
	}

	/**
	* Sets the title of this book.
	*
	* @param title the title of this book
	*/
	public void setTitle(java.lang.String title) {
		_book.setTitle(title);
	}

	/**
	* Returns the author of this book.
	*
	* @return the author of this book
	*/
	public java.lang.String getAuthor() {
		return _book.getAuthor();
	}

	/**
	* Sets the author of this book.
	*
	* @param author the author of this book
	*/
	public void setAuthor(java.lang.String author) {
		_book.setAuthor(author);
	}

	/**
	* Returns the description of this book.
	*
	* @return the description of this book
	*/
	public java.lang.String getDescription() {
		return _book.getDescription();
	}

	/**
	* Sets the description of this book.
	*
	* @param description the description of this book
	*/
	public void setDescription(java.lang.String description) {
		_book.setDescription(description);
	}

	/**
	* Returns the publisher of this book.
	*
	* @return the publisher of this book
	*/
	public java.lang.String getPublisher() {
		return _book.getPublisher();
	}

	/**
	* Sets the publisher of this book.
	*
	* @param publisher the publisher of this book
	*/
	public void setPublisher(java.lang.String publisher) {
		_book.setPublisher(publisher);
	}

	/**
	* Returns the pages of this book.
	*
	* @return the pages of this book
	*/
	public int getPages() {
		return _book.getPages();
	}

	/**
	* Sets the pages of this book.
	*
	* @param pages the pages of this book
	*/
	public void setPages(int pages) {
		_book.setPages(pages);
	}

	/**
	* Returns the price of this book.
	*
	* @return the price of this book
	*/
	public double getPrice() {
		return _book.getPrice();
	}

	/**
	* Sets the price of this book.
	*
	* @param price the price of this book
	*/
	public void setPrice(double price) {
		_book.setPrice(price);
	}

	public boolean isNew() {
		return _book.isNew();
	}

	public void setNew(boolean n) {
		_book.setNew(n);
	}

	public boolean isCachedModel() {
		return _book.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_book.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _book.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _book.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_book.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _book.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_book.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new BookWrapper((Book)_book.clone());
	}

	public int compareTo(it.unisef.managebooks.model.Book book) {
		return _book.compareTo(book);
	}

	@Override
	public int hashCode() {
		return _book.hashCode();
	}

	public com.liferay.portal.model.CacheModel<it.unisef.managebooks.model.Book> toCacheModel() {
		return _book.toCacheModel();
	}

	public it.unisef.managebooks.model.Book toEscapedModel() {
		return new BookWrapper(_book.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _book.toString();
	}

	public java.lang.String toXmlString() {
		return _book.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_book.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public Book getWrappedBook() {
		return _book;
	}

	public Book getWrappedModel() {
		return _book;
	}

	public void resetOriginalValues() {
		_book.resetOriginalValues();
	}

	private Book _book;
}