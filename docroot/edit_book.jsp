<%@ include file="/init.jsp" %>

<%@page import="it.unisef.managebooks.BookTitleException" %>
<%@page import="it.unisef.managebooks.BookAuthorException" %>

<%
	Book book =  (Book) request.getAttribute(WebKeys.BOOK);
%>

<liferay-ui:header title='<%= book != null ? book.getTitle() : "new-book" %>' backURL="<%= redirect %>" />

<portlet:actionURL name="updateBook" var="updateBookURL" >
	<portlet:param name="mvcPath" value="/edit_book.jsp" />
	<portlet:param name="redirect" value="<%= redirect %>" />
</portlet:actionURL>


<aui:form action="<%= updateBookURL %>" method="post" name="fm" >
	<aui:fieldset>

		<aui:input type="hidden" name="redirect" value="<%= redirect %>" />
		<aui:input type="hidden" name="bookId" value="<%= book == null ? StringPool.BLANK : book.getBookId() %>" />

		<liferay-ui:error exception="<%= BookTitleException.class %>" message="please-enter-a-valid-title" />
		<liferay-ui:error exception="<%= BookAuthorException.class %>" message="please-enter-a-valid-author" />

		<aui:model-context bean="<%= book %>" model="<%= Book.class %>" />

		<aui:input name="title" />
		<aui:input name="author" />

		<aui:input name="publisher" />
		<aui:input name="description" />

		<aui:input name="pages" />

		<aui:input name="price" />

	</aui:fieldset>

	<aui:button-row>
		<aui:button type="submit" value="save" />

		<aui:button type="cancel" onClick="<%= redirect %>" />
	</aui:button-row>


</aui:form>