<%@include file="/init.jsp" %>


<%@ page import="com.liferay.portal.kernel.dao.search.ResultRow"%>

<%
	ResultRow row = (ResultRow)request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
	Book book = (Book)row.getObject();

	long bookId = book.getBookId();
%>

<liferay-ui:icon-menu>
<%-- 	<c:if test="<%= TaskPermission.contains(permissionChecker, taskId, ActionKeys.UPDATE) %>"> --%>
			<portlet:renderURL var="updateBookURL">
				<portlet:param name="mvcPath" value="/edit_book.jsp"/>
				<portlet:param name="redirect" value="<%= currentURL %>"/>
				<portlet:param name="bookId" value="<%= String.valueOf(bookId) %>"/>
			</portlet:renderURL>

		<liferay-ui:icon image="edit" url="<%= updateBookURL.toString() %>"/>
<%-- 	</c:if> --%>
<%-- 	<c:if test="<%= TaskPermission.contains(permissionChecker, taskId, ActionKeys.PERMISSIONS) %>"> --%>
<%-- 			<liferay-security:permissionsURL --%>
<%-- 				modelResource="it.unisef.managebooks.model.Book" --%>
<%-- 				modelResourceDescription="<%= HtmlUtil.escape(book.getTitle()) %>" --%>
<%-- 				resourcePrimKey="<%= String.valueOf(bookId) %>" --%>
<%-- 				var="permissionsURL"> --%>
<%-- 			</liferay-security:permissionsURL> --%>
<%-- 		<liferay-ui:icon image="permissions" url="<%= permissionsURL.toString() %>"/> --%>
<%-- 	</c:if> --%>
<%-- 	<c:if test="<%= TaskPermission.contains(permissionChecker, taskId, ActionKeys.DELETE) %>"> --%>
		<portlet:actionURL name="deleteBook" var="deleteBookURL">
			<portlet:param name="bookId" value="<%= String.valueOf(bookId) %>"/>
			<portlet:param name="redirect" value="<%= currentURL %>"/>
		</portlet:actionURL>

	<liferay-ui:icon image="delete" url="<%=deleteBookURL.toString() %>"/>
<%-- 	</c:if> --%>

</liferay-ui:icon-menu>